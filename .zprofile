#!/bin/bash/zsh

#  ___  ___           _        _ _ _            _
#  |  \/  |          | |      | (_| |          (_)
#  | .  . | ___  _ __| |_ __ _| |_| |_ __ _ ___ _
#  | |\/| |/ _ \| '__| __/ _` | | | __/ _` / __| |
#  | |  | | (_) | |  | || (_| | | | || (_| \__ | |
#  \_|__|_/\___/|_|   \__\__,_|_|_|\__\__,_|___|_|
#
# Chris Mortalitasi
# https://gitlab.com/Mortalitasi

# --- .profile file
# --- runs on login. environmental variables are set here
# -------------------------------------------------

# ~/.local/bin` to $PATH
export PATH="$PATH:$(du "$HOME/.local/bin" | cut -f2 | paste -sd ':' -)"

# default programs
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWNSER="brave"

# home clean up
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_CACHE_HOME="$HOME/.cache"

export ZDOTDIR="${XDG_CONFIG_HOME:-$HOME/.config}/zsh"
export HISTFILE="${XDG_DATA_HOME:-$HOME/.local/share}/zsh/history"
export LESSHISTFILE="-"
export GTK2_RC_FILES="${XDG_CONFIG_HOME:-$HOME/.config}/gtk-2.0/gtkrc-2.0"
export PASSWORD_STORE_DIR="${XDG_DATA_HOME:-$HOME/.local/share}/password-store"
export GNUPGHOME="${XDG_DATA_HOME:-$HOME/.local/share}/gnupg"
export GOPATH="${XDG_DATA_HOME:-$HOME/.local/share}/go"
export SUDO_ASKPASS="$HOME/.local/bin/dmenupass"
export QT_QPA_PLATFORMTHEME="gtk2"	# Have QT use gtk2 theme

# other programs
export AWT_TOOLKIT="MToolkit wmname LG3D"	#May have to install wmname
export FZF_DEFAULT_OPTS="--layout=reverse --height 40%"
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"


# ------------------------------------------------
# ---
# ---
