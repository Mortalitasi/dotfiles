;  ___  ___           _        _ _ _            _
;  |  \/  |          | |      | (_| |          (_)
;  | .  . | ___  _ __| |_ __ _| |_| |_ __ _ ___ _
;  | |\/| |/ _ \| '__| __/ _` | | | __/ _` / __| |
;  | |  | | (_) | |  | || (_| | | | || (_| \__ | |
;  \_|__|_/\___/|_|   \__\__,_|_|_|\__\__,_|___|_|
;
; https://gitlab.com/Mortalitasi
; Chris Mortalitasi

; ---
; --- doom emacs config.
; ------------------------------------------------

;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;; Place your private configuration here! Remember, you do not need to run 'doom
;; sync' after modifying this file!
;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets.
(setq user-full-name "Chris Mortalitasi"
      user-mail-address "mortalitasi@protonmail.com")

;; doom Theme
;; setting the theme to doom-nord.
(setq doom-theme 'doom-nord)

;; fonts
(setq doom-font (font-spec :family "Source Code Pro" :size 15)
      doom-variable-pitch-font (font-spec :family "Ubuntu" :size 15)
      doom-big-font (font-spec :family "Source Code Pro" :size 24))
(after! doom-themes
  (setq doom-themes-enable-bold t
        doom-themes-enable-italic t))
(custom-set-faces!
  '(font-lock-comment-face :slant italic)
  '(font-lock-keyword-face :slant italic))

;; numbers and Lines
(setq display-line-numbers-type t)

;; dashboard
(use-package dashboard
  :init      ;; tweak dashboard config before loading it
  (setq dashboard-set-heading-icons t)
  (setq dashboard-set-file-icons t)
  ;;(setq dashboard-startup-banner 'logo) ;; use standard emacs logo as banner
  (setq dashboard-startup-banner "~/.config/doom/dash-logo/doom-emacs.svg")  ;; use custom image as banner
  (setq dashboard-center-content nil) ;; set to 't' for centered content
  (setq dashboard-items '((recents . 5)
                          ;;(agenda . 5 )
                        ;; (bookmarks . 5)
                          (projects . 5)))
                         ;; (registers . 5)
  :config
  (dashboard-setup-startup-hook)
  (dashboard-modify-heading-icons '((recents . "file-text")
                                    (bookmarks . "book"))))
;; dashboard in emacsclient
(setq doom-fallback-buffer-name "*dashboard*")

;; modeline
(set-face-attribute 'mode-line nil :font "Source Code Pro")
(setq doom-modeline-height 30     ;; sets modeline height
      doom-modeline-bar-width 5   ;; sets right bar width
      doom-modeline-persp-name t  ;; adds perspective name to modeline
      doom-modeline-persp-icon t  ;; adds folder icon next to persp name
      doom-modeline-project-detection 'auto  ;; detect the project root
      doom-modeline-buffer-file-name-style 'auto ;; Determines the style used by `doom-modeline-buffer-file-name'.
      doom-modeline-icon (display-graphic-p) ;; Whether display icons in the mode-line.
      doom-modeline-buffer-state-icon t  ;; Whether display the icon for the buffer state. It respects `doom-modeline-icon'.
      doom-modeline-major-mode-icon t  ;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
      doom-modeline-major-mode-color-icon t)  ;; Whether display the colorful icon for `major-mode'.

;; org mode
(map! :leader
      :desc "Org babel tangle" "m B" #'org-babel-tangle)
(after! org
  (setq org-directory "~/Documents/org/"
        org-agenda-files '("~/Documents/org/agenda.org")
        org-default-notes-file (expand-file-name "notes.org" org-directory)
        org-ellipsis " ▼ "
        org-superstar-headline-bullets-list '("◉" "●" "○" "◆" "●" "○" "◆")
        org-superstar-item-bullet-alist '((?+ . ?➤) (?- . ?✦)) ; changes +/- symbols in item lists
        org-log-done 'time
        org-hide-emphasis-markers t
        ;; ex. of org-link-abbrev-alist in action
        ;; [[arch-wiki:Name_of_Page][Description]]
        org-link-abbrev-alist    ; This overwrites the default Doom org-link-abbrev-list
          '(("arch-wiki" . "https://wiki.archlinux.org/index.php/")
            ("ddg" . "https://duckduckgo.com/?q=")
            ("wiki" . "https://en.wikipedia.org/wiki/"))
        org-todo-keywords        ; This overwrites the default Doom org-todo-keywords
          '((sequence
             "TODO(t)"           ; A task that is ready to be tackled
             "BLOG(b)"           ; Blog writing assignments
             "GYM(g)"            ; Things to accomplish at the gym
             "PROJ(p)"           ; A project that contains other tasks
             "VIDEO(v)"          ; Video assignments
             "WAIT(w)"           ; Something is holding up this task
             "|"                 ; The pipe necessary to separate "active" states and "inactive" states
             "DONE(d)"           ; Task has been completed
             "CANCELLED(c)" )))) ; Task has been cancelled

;; font sizes for each header level in Org
(custom-set-faces
  '(org-level-1 ((t (:inherit outline-1 :height 1.4))))
  '(org-level-2 ((t (:inherit outline-2 :height 1.3))))
  '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
  '(org-level-4 ((t (:inherit outline-4 :height 1.1))))
  '(org-level-5 ((t (:inherit outline-5 :height 1.0))))
)

;; org export
 (use-package ox-man)
 (use-package ox-gemini)

;; org journal
(setq org-journal-dir "~/Documents/org/journal/"
      org-journal-date-prefix "* "
      org-journal-time-prefix "** "
      org-journal-date-format "%d, %B %Y (%A) "
      org-journal-file-format "%d-%m-%Y.org")

;; neotree
(after! neotree
  (setq neo-smart-open t
        neo-window-fixed-size nil))
(after! doom-themes
  (setq doom-neotree-enable-variable-pitch t))
(map! :leader
      :desc "Toggle neotree file viewer" "t n" #'neotree-toggle
      :desc "Open directory in neotree" "d n" #'neotree-dir)

;; emojis
(use-package emojify
  :hook (after-init . global-emojify-mode))

;;centered align
(defun chris/org-mode-visual-fill ()
  (setq visual-fill-column-width 100
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . chris/org-mode-visual-fill))

; ------------------------------------------------
; ---
; ---
