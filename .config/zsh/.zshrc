#!/bin/zsh

#  ___  ___           _        _ _ _            _
#  |  \/  |          | |      | (_| |          (_)
#  | .  . | ___  _ __| |_ __ _| |_| |_ __ _ ___ _
#  | |\/| |/ _ \| '__| __/ _` | | | __/ _` / __| |
#  | |  | | (_) | |  | || (_| | | | || (_| \__ | |
#  \_|__|_/\___/|_|   \__\__,_|_|_|\__\__,_|___|_|
#
# https://gitlab.com/Mortalitasi
# Chris Mortalitasi

# ---
# --- zsh config.
# ------------------------------------------------

# enable colors.
export TERM="xterm-256color"

setopt autocd   # automatically cd into typed directory.
stty stop undef # disable ctrl-s to freeze terminal.
setopt interactive_comments

# history in cache directory.
HISTSIZE=3000
SAVEHIST=3000
HISTFILE=~/.cache/zsh/history

# emacs.
export PATH="$HOME/.config/emacs/bin:$PATH"

# load alias and shortcuts.
[ -f "${XDG_CONFIG_HOME:-$HOME/.config}/shell/alias" ] && source "${XDG_CONFIG_HOME:-$HOME/.config}/shell/alias"

# auto/tab complete.
autoload -U compinit
zstyle ':completion:*' menu select
zmodload zsh/complist
compinit
_comp_options+=(globdots)  # include hidden files.

# vi mode.
bindkey -v

# change cursor shape for different vi modes.
function zle-keymap-select {
  if [[ ${KEYMAP} == vicmd ]] ||
     [[ $1 = 'block' ]]; then
    echo -ne '\e[1 q'
  elif [[ ${KEYMAP} == main ]] ||
       [[ ${KEYMAP} == viins ]] ||
       [[ ${KEYMAP} = '' ]] ||
       [[ $1 = 'beam' ]]; then
    echo -ne '\e[5 q'
  fi
}
zle -N zle-keymap-select
zle-line-init() {
    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
    echo -ne "\e[5 q"
}
zle -N zle-line-init
echo -ne '\e[5 q' # use beam shape cursor on startup.
preexec() { echo -ne '\e[5 q' ;} # use beam shape cursor for each new prompt.


# change tittle of terminal
case ${TERM} in
  xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|alacritty|st|konsole*)
    PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
        ;;
  screen*)
    PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
    ;;
esac

# bare git repo alias for dotfiles
# alias config="/usr/bin/git --git-dir=$HOME/dotfiles --work-tree=$HOME"

# merge xresources
alias merge='xrdb -merge ~/.Xresources'

# load syntax highlighting; should be last.
 source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.plugin.zsh 2>/dev/null
 source /usr/share/zsh/plugins/fast-syntax-highlighting/fast-syntax-highlighting.plugin.zsh 3>/dev/null

# random color script
colorscript random

# starship prompt.
eval "$(starship init zsh)"

# time in prompt.
# RPROMPT="[%B%D{%f/%m/%y} | %D{%L:%M:%S}]"

# ------------------------------------------------
# ---
# ---
